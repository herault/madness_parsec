extern "C" %{
/*
 * Copyright (c) 2010-2016 The University of Tennessee and The University
 *                         of Tennessee Research Foundation. All rights
 *                         reserved.
 * Copyright (c) 2013-2016 Inria. All rights reserved.
 * $COPYRIGHT
 *
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

dataA     [type = "dague_ddesc_t *"]
dataT     [type = "dague_ddesc_t *"]
dataQ     [type = "dague_ddesc_t *"]
p_work    [type = "dague_memory_pool_t *" size = "((sizeof(dague_complex64_t))*ib)*descT.nb"]

descA     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
descT     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataT)"]
descQ     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataQ)"]
ib        [type = "int" hidden = on default = "descT.mb" ]
KT        [type = "int" hidden = on default = "descA.nt-1" ]
KT2       [type = "int" hidden = on default = "dplasma_imin( KT, descQ.mt-2 )" ]

zlaset(m, n) [profile = off]
  /* Execution Space */
  m  = 0 .. descQ.mt-1
  n  = 0 .. descQ.nt-1
  k  = inline_c %{ return dplasma_imin(KT,dplasma_imin(m, n)); %}
  cq = inline_c %{ return ((descQ.mt == descA.nt) & (m == (descQ.mt-1)) & (n == (descQ.mt-1))); %}

  /* Locality */
  : dataQ(m,n)

  READ  A    <- dataQ(m,n)
             ->  cq ? C zunmqr(k, n)
             -> (!cq & ((m <= KT) & (n >= m))) ? A1 ztsmqr(k, descQ.mt-1, n)
             -> (!cq & ((m >  KT) | (n <  m))) ? A2 ztsmqr(k, m,          n)

BODY
{
    int tempmm = (m == (descQ.mt-1)) ? (descQ.m - m * descQ.mb) : descQ.mb;
    int tempnn = (n == (descQ.nt-1)) ? (descQ.n - n * descQ.nb) : descQ.nb;
    int ldqm = BLKLDD( descQ, m );

    printlog("CORE_zlaset(%d, %d)\n"
             "\t(PlasmaUpperLower, tempmm, tempnn, alpha, (m == n) ? (beta) : (alpha), Q(%d,%d)[%p], ldqm)\n",
             m, n, m, n, A);

#if !defined(DAGUE_DRY_RUN)
    CORE_zlaset(PlasmaUpperLower, tempmm, tempnn,
                0., (m == n) ? 1.: 0.,
                A /* dataQ(m,n) */, ldqm );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

zunmqr(k, n)
  /* Execution Space */
  k = 0 .. KT
  n = k .. descQ.nt-1

  /* Locality */
  : dataQ(k, n)

  READ  A    <- A zunmqr_in_data_A0(k)   [type = LOWER_TILE]
  READ  T    <- T zunmqr_in_data_T1(k)   [type = LITTLE_T]

  RW    C    <- ( k == (descQ.mt-1)) ? A  zlaset(k, n)
             <- ( k <  (descQ.mt-1)) ? A1 ztsmqr(k, k+1, n)
             -> ( k == 0 ) ? dataQ(k, n)
             -> ( k >  0 ) ? A2 ztsmqr(k-1, k, n)

BODY
{
    int tempAkm  = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int tempAkn  = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int tempkmin = dplasma_imin( tempAkm, tempAkn );
    int tempkm   = (k == (descQ.mt-1)) ? (descQ.m - k * descQ.mb) : descQ.mb;
    int tempnn   = (n == (descQ.nt-1)) ? (descQ.n - n * descQ.nb) : descQ.nb;
    int ldak = BLKLDD( descA, k );
    int ldqk = BLKLDD( descQ, k );

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( p_work );

    CORE_zunmqr(PlasmaLeft, PlasmaNoTrans,
                tempkm, tempnn, tempkmin, ib,
                A /* dataA(k, k) */, ldak,
                T /* dataT(k, k) */, descT.mb,
                C /* dataQ(k, n) */, ldqk,
                p_elem_A, descT.nb );

    dague_private_memory_push( p_work, p_elem_A );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

zunmqr_in_data_T1(k) [profile = off]
  /* Execution Space */
  k = 0 .. KT

  /* Locality */
  : dataT(k,k)

  READ  T    <- dataT(k,k)                      [type = LITTLE_T]
             -> T zunmqr(k, k .. descQ.nt-1)    [type = LITTLE_T]

BODY
{
    /* nothing */
}
END

zunmqr_in_data_A0(k) [profile = off]
  /* Execution Space */
  k = 0 .. KT

  /* Locality */
  : dataA(k,k)

  READ  A    <- dataA(k,k)                      [type = LOWER_TILE]
             -> A zunmqr(k, k .. descQ.nt-1)    [type = LOWER_TILE]

BODY
{
    /* nothing */
}
END

ztsmqr(k, m, n)
  /* Execution Space */
  k = 0     .. KT
  m = (k+1) .. (descQ.mt-1)
  n = k     .. (descQ.nt-1)

  /* Locality */
  : dataQ(m,n)

  RW    A1   <- ( m == (descQ.mt-1) ) ? A  zlaset(k, n)
             <- ( m <  (descQ.mt-1) ) ? A1 ztsmqr(k, m+1, n)
             -> ( m == (k+1) ) ? C  zunmqr(k, n)
             -> ( m >  (k+1) ) ? A1 ztsmqr(k, m-1, n)

  RW    A2   <- ((k == KT) | (n == k)) ? A zlaset(m, n)
             <- ((k <  KT) & (n >  k) & (m == (k+1))) ? C  zunmqr(k+1, n)
             <- ((k <  KT) & (n >  k) & (m >  (k+1))) ? A2 ztsmqr(k+1, m, n)
             -> ( k == 0 ) ? dataQ(m, n)
             -> ( k >  0 ) ? A2 ztsmqr(k-1, m, n)

  READ  V    <- V ztsmqr_in_data_A1(k, m)
  READ  T    <- T ztsmqr_in_data_T2(k, m)  [type = LITTLE_T]

BODY
{
    int tempAkm  = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int tempAkn  = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int tempkmin = dplasma_imin( tempAkm, tempAkn );
    int tempmm   = (m == (descQ.mt-1)) ? (descQ.m - m * descQ.mb) : descQ.mb;
    int tempnn   = (n == (descQ.nt-1)) ? (descQ.n - n * descQ.nb) : descQ.nb;
    int ldam = BLKLDD( descA, m );
    int ldqk = BLKLDD( descQ, k );
    int ldqm = BLKLDD( descQ, m );

    printlog("CORE_ztsmqr(%d, %d, %d)\n"
             "\t(side, trans, descQ.mb, tempnn, tempmm, tempnn, tempkmin, ib, Q(%d,%d)[%p], ldbk, Q(%d,%d)[%p], ldbm, A(%d,%d)[%p], ldam, T(%d,%d)[%p], descT.mb, p_elem_A, ldwork)\n",
             k, m, n, k, n, A1, m, n, A2, m, k, V, m, k, T);

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( p_work );

    CORE_ztsmqr(PlasmaLeft, PlasmaNoTrans,
                descQ.mb, tempnn, tempmm, tempnn, tempkmin, ib,
                A1 /* dataQ(k, n) */, ldqk,
                A2 /* dataQ(m, n) */, ldqm,
                V  /* dataA(m, k) */, ldam,
                T  /* dataT(m, k) */, descT.mb,
                p_elem_A, ib );

    dague_private_memory_push( p_work, p_elem_A );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

ztsmqr_in_data_T2(k, m) [profile = off]
  /* Execution Space */
  k = 0     .. KT2
  m = (k+1) .. (descQ.mt-1)

  /* Locality */
  : dataT(m, k)

  READ  T    <- dataT(m, k)                         [type = LITTLE_T]
             -> T ztsmqr(k, m, k .. (descQ.nt-1))   [type = LITTLE_T]

BODY
{
    /* nothing */
}
END

ztsmqr_in_data_A1(k, m) [profile = off]
  /* Execution Space */
  k = 0     .. KT2
  m = (k+1) .. (descQ.mt-1)

  /* Locality */
  : dataA(m, k)

  READ  V    <- dataA(m, k)
             -> V ztsmqr(k, m, k .. (descQ.nt-1))

BODY
{
    /* nothing */
}
END
