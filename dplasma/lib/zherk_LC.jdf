extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> z c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

uplo      [type = "PLASMA_enum"]
trans     [type = "PLASMA_enum"]
alpha     [type = "double"]
dataA     [type = "dague_ddesc_t *"]
descA     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
beta      [type = "double"]
dataC     [type = "dague_ddesc_t *"]
descC     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataC)"]


zherk(n, k)
  /* Execution Space */
  n = 0..(descC.nt-1)
  k = 0..(descA.mt-1)

  /* Locality */
  : dataC(n,n)

  READ  A    <- A zherk_in_data_A0(n, k)
  RW    C    <- ((0==k)) ? dataC(n,n)
             <- ((k>=1)) ? C zherk(n, k-1)
             -> ((descA.mt>=(2+k))) ? C zherk(n, k+1)
             -> ((descA.mt==(1+k))) ? dataC(n,n)

BODY
{
    int tempnn = (n==(descC.nt-1)) ? (descC.n-(n*descC.nb)) : descC.nb;
    int tempkm = (k==(descA.mt-1)) ? (descA.m-(k*descA.mb)) : descA.mb;
    int ldak = BLKLDD( descA, k );
    double dbeta = (k==0) ? beta : (double)1.;
    int ldcn = BLKLDD( descC, n );

    printlog("CORE_zherk(%d, %d)\n"
             "\t(uplo, trans, tempnn, tempkm, alpha, A(%d,%d)[%p], ldak, dbeta, C(%d,%d)[%p], ldcn)\n",
             n, k, k, n, A, n, n, C);
#if !defined(DAGUE_DRY_RUN)
    CORE_zherk(uplo, trans, tempnn,
               tempkm, alpha, A /* dataA(k,n) */,
               ldak, dbeta, C /* dataC(n,n) */,
               ldcn );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zherk_in_data_A0(n, k) [profile = off]
  /* Execution Space */
  n = 0..(descC.nt-1)
  k = 0..(descA.mt-1)

  /* Locality */
  : dataA(k,n)

  READ  A    <- dataA(k,n)
             -> A zherk(n, k)

BODY
{
    /* nothing */
}
END

zgemm(n, m, k)
  /* Execution Space */
  n = 0..(descC.mt-2)
  m = (n+1)..(descC.mt-1)
  k = 0..(descA.mt-1)

  /* Locality */
  : dataC(m,n)

  READ  A    <- A zgemm_in_data_A0(m, k)
  READ  B    <- B zgemm_in_data_A1(n, k)
  RW    C    <- ((k>=1)) ? C zgemm(n, m, k-1)
             <- ((0==k)) ? dataC(m,n)
             -> ((descA.mt==(k+1))) ? dataC(m,n)
             -> ((descA.mt>=(2+k))) ? C zgemm(n, m, k+1)

BODY
{
    int tempmm = ((m)==(descC.mt-1)) ? (descC.m-(m*descC.mb)) : descC.mb;
    int tempnn = (n==(descC.nt-1)) ? (descC.n-(n*descC.nb)) : descC.nb;
    int tempkm = (k==(descA.mt-1)) ? (descA.m-(k*descA.mb)) : descA.mb;
    dague_complex64_t zalpha = (dague_complex64_t)alpha;
    int ldak = BLKLDD( descA, k );
    dague_complex64_t zbeta = (k==0) ? ((dague_complex64_t)beta) : ((double)1.);
    int ldcm = BLKLDD( descC, m );

    printlog("CORE_zgemm(%d, %d, %d)\n"
             "\t(trans, PlasmaNoTrans, tempmm, tempnn, tempkm, zalpha, A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldak, zbeta, C(%d,%d)[%p], ldcm)\n",
             n, m, k, k, m, A, k, n, B, m, n, C);
#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(trans, PlasmaNoTrans, tempmm,
               tempnn, tempkm, zalpha,
               A /* dataA(k,m) */, ldak, B /* dataA(k,n) */,
               ldak, zbeta, C /* dataC(m,n) */,
               ldcm );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zgemm_in_data_A1(n, k) [profile = off]
  /* Execution Space */
  n = 0..(descC.mt-2)
  k = 0..(descA.mt-1)

  /* Locality */
  : dataA(k,n)

  READ  B    <- dataA(k,n)
             -> B zgemm(n, (n+1)..(descC.mt-1), k)

BODY
{
    /* nothing */
}
END

zgemm_in_data_A0(m, k) [profile = off]
  /* Execution Space */
  m = 1..(descC.mt-1) /* tight bound is (n+1)..(descC.mt-1) */
  k = 0..(descA.mt-1)

  /* Locality */
  : dataA(k,m)

  READ  A    <- dataA(k,m)
             -> A zgemm(0..(descC.mt-2), m, k)

BODY
{
    /* nothing */
}
END
