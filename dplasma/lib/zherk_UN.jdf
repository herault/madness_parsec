extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> z c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

uplo      [type = "PLASMA_enum"]
trans     [type = "PLASMA_enum"]
alpha     [type = "double"]
dataA     [type = "dague_ddesc_t *"]
descA     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
beta      [type = "double"]
dataC     [type = "dague_ddesc_t *"]
descC     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataC)"]


zherk(n, k)
  /* Execution Space */
  n = 0..(descC.nt-1)
  k = 0..(descA.nt-1)

  /* Locality */
  : dataC(n,n)

  READ  A    <- A zherk_in_data_A0(n, k)
  RW    C    <- ((0==k)) ? dataC(n,n)
             <- ((k>=1)) ? C zherk(n, k-1)
             -> ((descA.nt>=(2+k))) ? C zherk(n, k+1)
             -> ((descA.nt==(k+1))) ? dataC(n,n)

BODY
{
    int tempnn = (n==(descC.nt-1)) ? (descC.n-(n*descC.nb)) : descC.nb;
    int tempkn = (k==(descA.nt-1)) ? (descA.n-(k*descA.nb)) : descA.nb;
    int ldan = BLKLDD( descA, n );
    double dbeta = (k==0) ? beta : ((double)1.);
    int ldcn = BLKLDD( descC, n );

    printlog("CORE_zherk(%d, %d)\n"
             "\t(uplo, trans, tempnn, tempkn, alpha, A(%d,%d)[%p], ldan, dbeta, C(%d,%d)[%p], ldcn)\n",
             n, k, n, k, A, n, n, C);
#if !defined(DAGUE_DRY_RUN)
    CORE_zherk(uplo, trans, tempnn,
               tempkn, alpha, A /* dataA(n,k) */,
               ldan, dbeta, C /* dataC(n,n) */,
               ldcn );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zherk_in_data_A0(n, k) [profile = off]
  /* Execution Space */
  n = 0..(descC.nt-1)
  k = 0..(descA.nt-1)

  /* Locality */
  : dataA(n,k)

  READ  A    <- dataA(n,k)
             -> A zherk(n, k)

BODY
{
    /* nothing */
}
END

zgemm(n, m, k)
  /* Execution Space */
  n = 0..(descC.mt-2)
  m = (n+1)..(descC.mt-1)
  k = 0..(descA.nt-1)

  /* Locality */
  : dataC(n,m)

  READ  A    <- A zgemm_in_data_A0(n, k)
  READ  B    <- B zgemm_in_data_A1(m, k)
  RW    C    <- ((0==k)) ? dataC(n,m)
             <- ((k>=1)) ? C zgemm(n, m, k-1)
             -> ((descA.nt>=(k+2))) ? C zgemm(n, m, k+1)
             -> ((descA.nt==(k+1))) ? dataC(n,m)

BODY
{
    int tempnn = (n==(descC.nt-1)) ? (descC.n-(n*descC.nb)) : descC.nb;
    int tempmm = ((m)==(descC.mt-1)) ? (descC.m-(m*descC.mb)) : descC.mb;
    int tempkn = (k==(descA.nt-1)) ? (descA.n-(k*descA.nb)) : descA.nb;
    dague_complex64_t zalpha = (dague_complex64_t)alpha;
    int ldan = BLKLDD( descA, n );
    int ldam = BLKLDD( descA, m );
    dague_complex64_t zbeta = (k==0) ? ((dague_complex64_t)beta) : ((double)1.);
    int ldcn = BLKLDD( descC, n );

    printlog("CORE_zgemm(%d, %d, %d)\n"
             "\t(trans, PlasmaConjTrans, tempnn, tempmm, tempkn, zalpha, A(%d,%d)[%p], ldan, A(%d,%d)[%p], ldam, zbeta, C(%d,%d)[%p], ldcn)\n",
             n, m, k, n, k, A, m, k, B, n, m, C);
#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(trans, PlasmaConjTrans, tempnn,
               tempmm, tempkn, zalpha,
               A /* dataA(n,k) */, ldan, B /* dataA(m,k) */,
               ldam, zbeta, C /* dataC(n,m) */,
               ldcn );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zgemm_in_data_A1(m, k) [profile = off]
  /* Execution Space */
  m = 1..(descC.mt-1) /* tight bound is (n+1)..(descC.mt-1) */
  k = 0..(descA.nt-1)

  /* Locality */
  : dataA(m,k)

  READ  B    <- dataA(m,k)
             -> B zgemm(0..(descC.mt-2), m, k)

BODY
{
    /* nothing */
}
END

zgemm_in_data_A0(n, k) [profile = off]
  /* Execution Space */
  n = 0..(descC.mt-2)
  k = 0..(descA.nt-1)

  /* Locality */
  : dataA(n,k)

  READ  A    <- dataA(n,k)
             -> A zgemm(n, (n+1)..(descC.mt-1), k)

BODY
{
    /* nothing */
}
END
